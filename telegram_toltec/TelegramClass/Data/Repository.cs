﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using telegram_toltec.Models;
using telegram_toltec.Types;

namespace telegram_toltec.TelegramClass.Data
{
    public static class Repository
    {
        public static long EndOfDay( this DateTime date )
        {
            return ((DateTimeOffset)new DateTime( date.Year, date.Month, date.Day, 23, 59, 59, 999 )).ToUnixTimeSeconds( );
        }

        public static long StartOfDay( this DateTime date )
        {
            return ((DateTimeOffset)new DateTime( date.Year, date.Month, date.Day, 0, 0, 0, 0 )).ToUnixTimeSeconds( );
        }

        public static async Task<User> UserGet( BotContext db, int UserId, string FirstName, string LastName, string Username )
        {
            User user = await db.Users.FirstOrDefaultAsync( p => p.UserId == UserId );

            if (user == null)
            {
                user = new Models.User
                {
                    UserId = UserId,
                    FirstName = FirstName,
                    LastName = LastName,
                    Username = Username
                };
                db.Users.Add( user );
                await db.SaveChangesAsync( );
            }

            return user;
        }

        public static async Task<User> UserSetPhone( BotContext db, int UserId, string Phone )
        {
            User user = await db.Users.FirstOrDefaultAsync( p => p.UserId == UserId );

            if (user != null)
            {
                user.Phone = Phone;
                db.Users.Update( user );
                await db.SaveChangesAsync( );
            }

            return user;
        }

        public static async Task<Chat> ChatGet( BotContext db, long ChatId, DateTime MessageDate, int MessageId, int UserId )
        {


            Chat chat = await db.Chats.FirstOrDefaultAsync( p => (p.ChatId == ChatId) );

            if (chat == null)
            {
                chat = new Chat
                {
                    ChatId = ChatId,
                    MessageId = MessageId,
                    UserId = UserId,
                    Date = MessageDate
                };
                db.Chats.Add( chat );
            }
            else
            {
                chat.MessageId = MessageId;
                chat.UserId = UserId;
                chat.Date = MessageDate;
                db.Chats.Update( chat );
            }

            await db.SaveChangesAsync( );
            return chat;
        }


        public static async Task ChatSet( BotContext db, long ChatId, int MessageId, Proces proces = Proces.Nothing )
        {
            Chat chat = await db.Chats.FirstOrDefaultAsync( p => (p.ChatId == ChatId) );

            if (chat == null)
            {
                chat = new Chat
                {
                    ChatId = ChatId,
                    MessageId = MessageId,
                    LastProces = Proces.Nothing,
                    Proces = proces
                };
                db.Chats.Add( chat );
            }
            else
            {
                chat.ChatId = ChatId;
                chat.MessageId = MessageId;
                chat.Proces = ((proces != Proces.Nothing) ? proces : chat.Proces);
                chat.LastProces = chat.Proces;

                db.Chats.Update( chat );
            }
            await db.SaveChangesAsync( );
        }



    }
}
