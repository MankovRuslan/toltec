﻿using telegram_toltec.Models;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Collections;
using Microsoft.EntityFrameworkCore;

namespace telegram_toltec.TelegramClass.Data
{

    public interface IChatRepository
    {
    }

    public class ChatRepository : IChatRepository
    {
        private BotContext _context = null;


        public ChatRepository( BotContext context )
        {
            _context = context;
        }

        public async Task<IEnumerable> GetList()
        {
            return await _context.Chats.ToListAsync( );
        }

        public async Task<Chat> Chat( int id )
        {
            return await _context.Chats.FirstOrDefaultAsync( p => p.ChatId == id );
        }

        public async Task Update( Chat chat )

        {
            _context.Chats.Update( chat );
            await _context.SaveChangesAsync( );
        }
    }
}
