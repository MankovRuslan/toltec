﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;
using telegram_toltec.Types;
using telegram_toltec.TelegramClass.Data;

namespace telegram_toltec.TelegramClass.Commands
{
    public class SelectLicensing : Command
    {
        public override string Name => "edrpou";

        public override async Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;


            var keyboard = new InlineKeyboardMarkup( new[]
                          {
                    new[] // first row
                    {
                        new InlineKeyboardCallbackButton("M.E.DOC (ЗВІТНІСТЬ)",$"licensing#{messageId}"),
                        new InlineKeyboardCallbackButton("M.E.DOC (ЗАРПЛАТА)",$"licensing#{messageId}")
                    },
                    new[] {
                        new InlineKeyboardCallbackButton("M.E.DOC (ЕДО)",$"licensing#{messageId}"),
                        new InlineKeyboardCallbackButton("M.E.DOC (БАНКІВСЬКІ РАХУНКИ)","gfgg")
                    },
                    new[] {
                        new InlineKeyboardCallbackButton("СОТА",$"licensing#{messageId}"),
                        new InlineKeyboardCallbackButton("Ключі","gfgg")
                    }
                } );


            return await client.SendTextMessageAsync( chatId, $"Выберите лицензию", replyMarkup: keyboard );
        }
    }
}
