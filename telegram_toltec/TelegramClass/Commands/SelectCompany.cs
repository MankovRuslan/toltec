﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;
using telegram_toltec.Types;
using telegram_toltec.TelegramClass.Data;

namespace telegram_toltec.TelegramClass.Commands
{
    public class SelectCompany : Command
    {
        public override string Name => "company";

        public override async Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;

            var keyboard = new InlineKeyboardMarkup( new[]
                          {
                    new[] // first row
                    {
                        new InlineKeyboardCallbackButton("Будсервис БЦ Контрагент не наш",$"company#{messageId}"),
                    },
                    new[] {
                        new InlineKeyboardCallbackButton("Мебитар",$"company#{messageId}")
                    },
                    new[] {
                        new InlineKeyboardCallbackButton("Коломієць ФОП",$"company#{messageId}")
                    },
                    new[] {
                        new InlineKeyboardCallbackButton("Аптека 1",$"company#{messageId}")
                    }
                } );


            return await client.SendTextMessageAsync( chatId, $"Выберите предприятие", replyMarkup: keyboard );
        }
    }
}
