﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.ReplyMarkups;
using telegram_toltec.Types;
using telegram_toltec.TelegramClass.Data;

namespace telegram_toltec.TelegramClass.Commands
{
    public class HelloCommand : Command
    {
        public override string Name => "start";

        public override async Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;

            return await client.SendTextMessageAsync( chatId, $"Добрий день! {data.FirstName}  Введіть ваш код <b>ЕДРПОУ</b>", parseMode: Telegram.Bot.Types.Enums.ParseMode.Html );
        }
    }
}
