﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegram_toltec.Types;

namespace telegram_toltec.TelegramClass.Commands
{
    public class AnyCommand : Command
    {
        public override string Name => "Any";

        public override async Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;
            var userId = data.UserName;
            return await client.SendTextMessageAsync( chatId, $"{chatId} / {messageId} / {userId} / {data.Data}", replyToMessageId: messageId );
        }

    }
}
