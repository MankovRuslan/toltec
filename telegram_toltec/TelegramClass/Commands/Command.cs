﻿using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using telegram_toltec.Models;
using telegram_toltec.Types;

namespace telegram_toltec.TelegramClass.Commands
{
    public abstract class Command
    {
        public abstract string Name
        {
            get;
        }

        public abstract Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command );

        public bool Contains( string command )
        {
            return command.Contains( this.Name );// && command.Contains( AppSettings.Name );
        }

    }
}
