﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineKeyboardButtons;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.InputMessageContents;
using Telegram.Bot.Types.ReplyMarkups;
using telegram_toltec.Types;

namespace telegram_toltec.TelegramClass.Commands
{
    public class PhoneCheckCommand : Command
    {
        public override string Name => "phone";

        public override async Task<Message> Execute( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;

            var keyboard = new ReplyKeyboardMarkup( new[]
                          {
                    new[] // first row
                    {
                        new KeyboardButton("Отправить номер телефона?"){ RequestContact = true},
                        new KeyboardButton("Отказ")
                    }
                } );

            return await client.SendTextMessageAsync( chatId, $"Для аутентификации <b>Подтвердите отправку номера телефона</b>", replyMarkup: keyboard, parseMode: ParseMode.Html );
        }

        public async Task<Message> Ok( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;

            var kb = new ReplyKeyboardRemove( );
            return await client.SendTextMessageAsync( chatId, $"Дякуємо", replyMarkup: kb );

        }

        public async Task<bool> Cancel( ExchangeData data, TelegramBotClient client, string command )
        {
            var chatId = data.ChatId;
            var messageId = data.MessageId;

            return await client.LeaveChatAsync( chatId );
        }
    }
}
