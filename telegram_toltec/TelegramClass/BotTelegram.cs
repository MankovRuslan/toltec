﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using telegram_toltec.Models;
using telegram_toltec.Types;
using telegram_toltec.TelegramClass.Commands;
using telegram_toltec.TelegramClass.Data;
using System.Text.RegularExpressions;
using System;

namespace telegram_toltec.TelegramClass
{
    public static class BotTelegram
    {


        private static TelegramBotClient client;
        public static int offset;
        public static int messageId;


        public static async Task<TelegramBotClient> Get()
        {
            if (client != null)
                return client;


            client = new TelegramBotClient( AppSettings.Key );
            var hook = string.Format( AppSettings.Url, "api/message/update" );

            await client.SetWebhookAsync( hook );

            return client;
        }

        public static async Task<bool> Update( TelegramBotClient client, Update update, BotContext db )
        {


            switch (update.Type)
            {
                case UpdateType.CallbackQueryUpdate:
                    {
                        var callback = update.CallbackQuery;
                        var textCommand = callback.Data.Substring( 1 );
                        var message = callback.Message;

                        ExchangeData data = new ExchangeData
                        {
                            ChatId = callback.Message.Chat.Id,
                            MessageId = callback.Message.MessageId,
                            MesageDate = callback.Message.Date,
                            UserId = callback.From.Id,
                            UserName = callback.From.Username,
                            FirstName = callback.From.FirstName,
                            LastName = callback.From.LastName
                        };

                        var sep = callback.Data.IndexOf( "#" );
                        data.Command = callback.Data.Substring( 1, sep - 2 );
                        data.Data = callback.Data.Substring( sep + 1 );

                        await UpdateProces( data, db );


                        break;
                    }
                case UpdateType.MessageUpdate:
                    {
                        var message = update.Message;

                        switch (message.Type)
                        {
                            case MessageType.TextMessage:
                                {

                                    ExchangeData data = new ExchangeData
                                    {
                                        ChatId = message.Chat.Id,
                                        MessageId = message.MessageId,
                                        MesageDate = message.Date,
                                        UserId = message.From.Id,
                                        UserName = message.From.Username,
                                        FirstName = message.From.FirstName,
                                        LastName = message.From.LastName,
                                        Data = message.Text
                                    };

                                    if (message.Text.StartsWith( "/" ))
                                    {
                                        data.Command = message.Text.ToLower( ).Substring( 1 );
                                    }
                                    else
                                    {
                                        data.Command = message.Text.ToLower( );
                                    }

                                    await UpdateProces( data, db );


                                    //var user = await Repository.UserGet( db, message.From.Id, message.From.FirstName, message.From.LastName, message.From.Username );
                                    //var BotChat = await Repository.ChatGet( db, message.Chat.Id, message.Date, message.MessageId, user.Id );

                                    //if (data.Command.StartsWith( "/start" ))
                                    //{
                                    //    var sended_message = await (new HelloCommand( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                                    //    await Repository.ChatSetMessage( db, sended_message.Chat.Id, sended_message.MessageId );
                                    //}
                                    //else if (message.Text.ToLower( ).StartsWith( "/phone" ))
                                    //{
                                    //    var sended_message = await (new PhoneCheckCommand( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                                    //    messageId = sended_message.MessageId;
                                    //    await Repository.ChatSetMessage( db, sended_message.Chat.Id, sended_message.MessageId );

                                    //}
                                    //if (message.Text.ToLower( ).StartsWith( "отказ" ))
                                    //{
                                    //    await (new PhoneCheckCommand( )).Cancel( data, client, BotChat.MessageId.ToString( ) );
                                    //}
                                    //else
                                    //{
                                    //    var sended_message = await (new PhoneCheckCommand( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                                    //    messageId = sended_message.MessageId;
                                    //    await Repository.ChatSetMessage( db, sended_message.Chat.Id, sended_message.MessageId );
                                    //}
                                    break;
                                }
                            case MessageType.ContactMessage:
                                {
                                    ExchangeData data = new ExchangeData
                                    {
                                        ChatId = message.Chat.Id,
                                        MessageId = message.MessageId,
                                        MesageDate = message.Date,
                                        UserId = message.From.Id,
                                        UserName = message.From.Username,
                                        FirstName = message.From.FirstName,
                                        LastName = message.From.LastName,
                                        Command = "contact",
                                        Data = message.Contact.PhoneNumber
                                    };

                                    await UpdateProces( data, db );
                                    //var textCommand = message.Contact.PhoneNumber;
                                    //var user = await Repository.UserSetPhone( db, message.From.Id, message.Contact.PhoneNumber );

                                    //await (new PhoneCheckCommand( )).Ok( data, client, "" );
                                    //var sended_message = await (new SelectCompany( )).Execute( data, client, "" );
                                    //messageId = sended_message.MessageId;
                                    break;
                                }
                        }
                        break;
                    }

            }

            return true;
        }

        public static async Task UpdateProces( ExchangeData data, BotContext db )
        {

            #region Стартовая фиксация/обработка сообщения 
            //save user information
            var user = await Repository.UserGet( db, data.UserId, data.FirstName, data.LastName, data.UserName );
            //save Chat information
            var BotChat = await Repository.ChatGet( db, data.ChatId, data.MesageDate, data.MessageId, data.UserId );

            var proces = BotChat.Proces;
            var lastProces = BotChat.LastProces;

            if (data.Command.StartsWith( "start" ))
            {
                proces = Proces.Start;
                lastProces = Proces.Nothing;
            }
            else if (data.Command.StartsWith( "phone" ))
            {
                proces = Proces.PhoneAsk;

            }
            else if (data.Command.StartsWith( "отказ" ))
            {
                proces = Proces.Start;
                lastProces = Proces.Nothing;
            }

            #endregion

            #region Фиксация события
            switch (lastProces)
            {
                case Proces.PhoneAsk:
                    {
                        string pattern = @"\(?\d{3}\)?[-\.]? *\d{3}[-\.]? *[-\.]?\d{4}";
                        Regex regex = new Regex( pattern, RegexOptions.IgnoreCase );

                        if (regex.IsMatch( data.Data.Trim( ) ))
                        {
                            await Repository.UserSetPhone( db, data.UserId, data.Data );
                            await (new PhoneCheckCommand( )).Ok( data, client, "" );
                        }
                        else
                        {
                            proces = Proces.Start;
                            lastProces = Proces.Nothing;
                        }
                        break;
                    }
                case Proces.EdrpouAsk:
                    {
                        String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
                        Regex objNumberPattern = new Regex( "(" + strValidIntegerPattern + ")" );

                        if (data.Data.Trim( ).Length == 11 || data.Data.Trim( ).Length == 8 && objNumberPattern.IsMatch( data.Data.Trim( ) ))
                        {
                            //TODO Save or Update Company user
                        }
                        else
                        {
                            //В начало
                            proces = Proces.Start;
                        }
                        break;
                    }
                case Proces.SelectCompany:
                    {
                        break;
                    }
                case Proces.SelectLicensing:
                    {
                        break;
                    }
            }

            #endregion

            #region Новое событие
            switch (proces)
            {
                case Proces.Start:
                    {
                        var sended_message = await (new HelloCommand( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                        await Repository.ChatSet( db, sended_message.Chat.Id, sended_message.MessageId, Proces.PhoneAsk );
                        break;
                    }
                case Proces.PhoneAsk:
                    {
                        var sended_message = await (new PhoneCheckCommand( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                        messageId = sended_message.MessageId;
                        await Repository.ChatSet( db, sended_message.Chat.Id, sended_message.MessageId, Proces.SelectCompany );
                        break;
                    }
                case Proces.EdrpouAsk:
                    {
                        break;
                    }
                case Proces.SearchCompany:
                    {
                        break;
                    }
                case Proces.SelectCompany:
                    {
                        var sended_message = await (new SelectCompany( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                        await Repository.ChatSet( db, sended_message.Chat.Id, sended_message.MessageId, Proces.SelectLicensing );
                        break;
                    }
                case Proces.SelectLicensing:
                    {
                        var sended_message = await (new SelectLicensing( )).Execute( data, client, BotChat.MessageId.ToString( ) );
                        await Repository.ChatSet( db, sended_message.Chat.Id, sended_message.MessageId, Proces.Start );
                        break;
                    }
                case Proces.SelectPeriodLicensing:
                    {
                        break;
                    }
                case Proces.SelectTypeKey:
                    {
                        break;
                    }
            }
            #endregion

        }
    }
}