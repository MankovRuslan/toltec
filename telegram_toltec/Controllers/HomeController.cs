﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using telegram_toltec.Models;
using telegram_toltec.TelegramClass;

namespace telegram_toltec.Controllers
{
    public class HomeController : Controller
    {

        private BotContext db;

        public HomeController( BotContext db )
        {
            this.db = db;
        }

        public IActionResult Index()
        {

            return Content( "Hello" );
        }

        public async Task<ContentResult> Update()
        {
            var client = new Telegram.Bot.TelegramBotClient( AppSettings.Key );
            await client.SetWebhookAsync( "" );
            var updates = await client.GetUpdatesAsync( BotTelegram.offset ); // получаем массив обновлений
            var testString = "<HTML> < HEAD ><META HTTP-EQUIV=\"refresh\" CONTENT=\"10\"> </ HEAD > < BODY ></ BODY ></ HTML>";


            foreach (var update in updates) // Перебираем все обновления
            {

                await BotTelegram.Update( client, update, db );

                BotTelegram.offset = update.Id + 1;
            }

            return Content( testString );
        }
    }

}

