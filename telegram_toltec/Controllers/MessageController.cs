﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using telegram_toltec.Models;
using telegram_toltec.TelegramClass;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace telegram_toltec.Controllers
{
    public class MessageController : Controller
    {

        private BotContext db;

        public MessageController( BotContext db )
        {
            this.db = db;
        }

        [Route( @"api/message/update" )]
        public async Task<OkResult> Update( [FromBody]Update update )
        {
            var client = await BotTelegram.Get( );

            await BotTelegram.Update( client, update, db );

            return Ok( );
        }
    }
}
