﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace telegram_toltec.Types
{
    public struct ExchangeData
    {
        public long ChatId;
        public int MessageId;
        public DateTime MesageDate;
        public int ReplayMessageId;
        public int UserId;
        public string UserName;
        public string FirstName;
        public string LastName;
        public string Command;
        public string Data;
    }
}
