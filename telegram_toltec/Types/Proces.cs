﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace telegram_toltec.Types
{
    public enum Proces
    {
        Nothing,
        Start,
        PhoneAsk,
        UserIdentify,
        EdrpouAsk,
        SelectCompany,
        SearchCompany,
        SelectLicensing,
        SelectPeriodLicensing,
        SelectTypeKey

    }
}
