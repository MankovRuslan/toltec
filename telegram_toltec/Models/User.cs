﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata;

namespace telegram_toltec.Models
{
    public class User
    {
        [Key]
        public int Id
        {
            get; set;
        }

        public int? UserId
        {
            get; set;
        }

        public string FirstName
        {
            get; set;
        }
        public string LastName
        {
            get; set;
        }
        public string Username
        {
            get; set;
        }
        public string Language_code
        {
            get; set;
        }
        public string Phone
        {
            get; set;
        }

        public List<Company> Companies
        {
            get; set;
        }

        public long? ChatId
        {
            get; set;
        }


    }
}
