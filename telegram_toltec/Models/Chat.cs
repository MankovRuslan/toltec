﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using telegram_toltec.Types;

namespace telegram_toltec.Models
{
    public class Chat
    {
        [DatabaseGenerated( DatabaseGeneratedOption.None ), Key]
        public int Id
        {
            get; set;
        }
        public long? ChatId
        {
            get; set;
        }
        public int? MessageId
        {
            get; set;
        }
        public DateTime? Date
        {
            get; set;
        }

        public string CommandName
        {
            get; set;
        }
        public int Level
        {
            get; set;
        }

        public Proces Proces
        {
            get; set;
        }

        public Proces LastProces
        {
            get; set;
        }

        public int? UserId
        {
            get; set;
        }


    }
}
