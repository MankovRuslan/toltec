﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace telegram_toltec.Models
{
    public class BotSetting
    {
        public int Id
        {
            set; get;
        }
        public string Name
        {
            set; get;
        }
        public int Offset
        {
            set; get;
        }
    }
}
