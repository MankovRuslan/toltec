﻿using Microsoft.EntityFrameworkCore;

namespace telegram_toltec.Models
{
    public class BotContext : DbContext
    {
        public BotContext( DbContextOptions options ) : base( options )
        {
        }

        public DbSet<User> Users
        {
            get; set;
        }
        public DbSet<Company> Companies
        {
            get; set;
        }
        public DbSet<BotSetting> BotSettings
        {
            get; set;
        }
        public DbSet<Chat> Chats
        {
            get; set;
        }
    }
}
