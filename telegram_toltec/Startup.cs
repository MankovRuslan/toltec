﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Telegram.Bot;
using telegram_toltec.Models;
using telegram_toltec.TelegramClass;

namespace telegram_toltec
{
    public class Startup
    {
        public Startup( IConfiguration configuration )
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration
        {
            get;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices( IServiceCollection services )
        {

            string connection = Configuration.GetConnectionString( "DefaultConnection" );
            services.AddDbContext<BotContext>( options => options.UseSqlServer( connection ) );

            BotTelegram.Get( );
            services.AddMvc( );
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure( IApplicationBuilder app, IHostingEnvironment env )
        {
            if (env.IsDevelopment( ))
            {
                app.UseDeveloperExceptionPage( );
                app.UseBrowserLink( );
            }
            else
            {
                app.UseExceptionHandler( "/Home/Error" );
            }

            app.UseStaticFiles( );

            //app.UseMvc( );
            app.UseMvc( routes =>
            {
                routes.MapRoute( "api", "api/update", new
                {
                    controler = "Message",
                    action = "update"
                } );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}" );
            } );
        }
    }
}
